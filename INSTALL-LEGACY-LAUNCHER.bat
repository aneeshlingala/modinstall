@echo off

echo Welcome to the Forge Mods installer by Aneesh Lingala!
echo Click install to install on the next screen.
del "%appdata%\.tlauncher\legacy\Minecraft\game\mods"
cls
echo "Extracting mods..."
mkdir "%appdata%\.tlauncher\legacy\Minecraft\game\mods"
powershell -Command "& {Expand-Archive -Force mods.zip '%appdata%\.tlauncher\legacy\Minecraft\game\mods\'}"
mshta javascript:alert("NOTE: In Legacy Launcher, choose Forge 1.20! Do not choose any older or newer versions, or the server will not work.");close();
