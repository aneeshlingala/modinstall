@echo off

echo Welcome to the Forge Mods installer by Aneesh Lingala!
echo Click install to install on the next screen.
del "%appdata%\.minecraft\mods"
echo Installing Forge...
java -jar forge.jar
cls
echo "Extracting mods..."
mkdir "%appdata%\.minecraft\mods"
powershell -Command "& {Expand-Archive -Force mods.zip '%appdata%\.minecraft\mods\'}"
mshta javascript:alert("NOTE: In the Minecraft Launcher, choose Forge 1.20! Do not choose any older or newer versions, or the server will not work.");close();
